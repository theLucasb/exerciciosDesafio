def calcular(numero_inserido):
    numero_ant = 0
    numero_prox = 1
    resultado = 0

    print(numero_ant, end=" ")
    print(numero_prox, end=" ")

    while (True):
        resultado = numero_ant + numero_prox
        numero_ant = numero_prox
        numero_prox = resultado

        print(resultado, end=' ')

        if (resultado == numero_inserido):
            return True

        if (resultado > numero_inserido):
            return False


def inserir_numero():
    print('Informe um número: ', end='')
    return int(input())


def main():
    while (True):
        numero_inserido = inserir_numero()

        pertence_fibonacci = calcular(numero_inserido)

        print('\n')

        if (pertence_fibonacci):
            print(numero_inserido, 'pertence à Série de Fibonacci')
        else:
            print(numero_inserido, 'não pertence à Série de Fibonacci')

        print('Informe outro número:')


main()
calcular()
